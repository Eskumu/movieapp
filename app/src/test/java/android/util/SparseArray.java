package android.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class SparseArray<E> {

    private LinkedHashMap<Integer, E> mHashMap;

    public SparseArray() {
        mHashMap = new LinkedHashMap<>();
    }

    public void put(int key, E value) {
        mHashMap.put(key, value);
    }

    public E get(int key, E defaultKey) {
        return mHashMap.get(key) == null ? defaultKey : mHashMap.get(key);
    }

    public E get(int key) {
        return mHashMap.get(key);
    }

    public int size() {
        return mHashMap.size();
    }

    public E valueAt(int i) {
        return new ArrayList<>(mHashMap.values()).get(i);

    }
}
