package ee.ttu.krtark.movieapp;

import android.util.SparseArray;

import org.junit.Before;
import org.junit.Test;

import ee.ttu.krtark.movieapp.DataBaseObjects.MovieCategory;
import ee.ttu.krtark.movieapp.DataBaseObjects.Movie;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by Kristjan on 20.04.2018.
 */
public class MovieDAOTest {

    private SparseArray<Movie> movies;
    private SparseArray<MovieCategory> categories;

    @Before
    public void setUp() {
        movies = new SparseArray<>();

        Movie movie1 = new Movie();
        movie1.setTitle("Title1");
        movie1.setId(1);

        Movie movie2 = new Movie();
        movie2.setTitle("Title2");
        movie2.setId(2);

        Movie movie3 = new Movie();
        movie3.setTitle("Title3");
        movie3.setId(3);

        movies.put(1, movie1);
        movies.put(2, movie2);
        movies.put(3, movie3);

        categories = new SparseArray<>();

        MovieCategory action = new MovieCategory();
        action.setId(1);
        action.setCategoryName("Action");

        MovieCategory drama = new MovieCategory();
        drama.setId(2);
        drama.setCategoryName("Drama");

        categories.put(action.getId(), action);
        categories.put(drama.getId(), drama);
    }

    @Test
    public void findAllMovies() {
        final int[] count = {0};


        MovieDAO db = new MovieDAO() {
            @Override
            public void onMovieReceivedFromDB(Movie movie) {
                assertEquals(movies.get(movie.getId()), movie);
                count[0]++;
            }

            @Override
            public void onCategoryReceivedFromDB(MovieCategory movieCategory) {
                fail();
            }
        };
        db.setupForTesting(movies, categories);
        // Custom Async task in test folder forces it to run synchronously.
        db.findAllMovies();
        assertEquals(3, count[0]);
    }

    @Test
    public void findMovieByID() {
        final int testID = 2;

        MovieDAO db = new MovieDAO() {
            @Override
            public void onMovieReceivedFromDB(Movie movie) {
                assertEquals(testID, movie.getId());
                assertEquals(movies.get(testID), movie);
            }

            @Override
            public void onCategoryReceivedFromDB(MovieCategory movieCategory) {
                fail();
            }
        };
        // Custom Async task in test folder forces it to run synchronously.
        db.setupForTesting(movies, categories);
        db.findMovieByID(testID);
    }

    @Test
    public void findMovieCategoryByID() {
        final int testID = 2;

        MovieDAO db = new MovieDAO() {
            @Override
            public void onMovieReceivedFromDB(Movie movie) {
                fail();
            }

            @Override
            public void onCategoryReceivedFromDB(MovieCategory movieCategory) {
                assertEquals(testID, movieCategory.getId());
                assertEquals(categories.get(testID).getCategoryName(), movieCategory.getCategoryName());
            }
        };
        // Custom Async task in test folder forces it to run synchronously.
        db.setupForTesting(movies, categories);
        db.findMovieCategoryByID(testID);
    }

    @Test
    public void findMovieByIDMissingMovie() {
        final int testID = 999;

        MovieDAO db = new MovieDAO() {
            @Override
            public void onMovieReceivedFromDB(Movie movie) {
                assertEquals(null, movie);
            }

            @Override
            public void onCategoryReceivedFromDB(MovieCategory movieCategory) {
                fail();
            }
        };
        // Custom Async task in test folder forces it to run synchronously.
        db.setupForTesting(movies, categories);
        db.findMovieByID(testID);
    }

    @Test
    public void findMovieCategoryByIDMissingCategory() {
        final int testID = 999;

        MovieDAO db = new MovieDAO() {
            @Override
            public void onMovieReceivedFromDB(Movie movie) {
                fail();
            }

            @Override
            public void onCategoryReceivedFromDB(MovieCategory movieCategory) {
                assertEquals(null, movieCategory);
            }
        };
        // Custom Async task in test folder forces it to run synchronously.
        db.setupForTesting(movies, categories);
        db.findMovieCategoryByID(testID);
    }
}