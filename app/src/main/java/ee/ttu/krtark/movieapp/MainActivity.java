package ee.ttu.krtark.movieapp;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ee.ttu.krtark.movieapp.MovieList.MovieListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        replaceFragmentNoStack(new MovieListFragment());
    }

    public void replaceFragmentNoStack(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.MainFrame, fragment);
        ft.commit();
    }

    public void replaceFragmentAddStack(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.MainFrame, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}
