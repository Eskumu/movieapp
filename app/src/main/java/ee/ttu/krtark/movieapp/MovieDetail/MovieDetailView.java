package ee.ttu.krtark.movieapp.MovieDetail;

import ee.ttu.krtark.movieapp.ViewMvc;

/**
 * Interface for View to show movie details
 */
interface MovieDetailView extends ViewMvc {
    /**
     * Add movie to the view.
     *
     * @param movie the movie to show
     */
    void bindMovie(MovieDetailObject movie);

    /**
     * Action to be taken, when movie is ready to display.
     * <p>
     * Should be called after bindMovie
     */
    void onMovieReady();
}
