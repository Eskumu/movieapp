package ee.ttu.krtark.movieapp.DataBaseObjects;

public class MovieCategory {
    private String categoryName;
    private int id;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
