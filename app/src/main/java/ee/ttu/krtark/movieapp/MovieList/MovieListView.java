package ee.ttu.krtark.movieapp.MovieList;

import ee.ttu.krtark.movieapp.ViewMvc;

/**
 * Interface for displaying movie lists
 */
interface MovieListView extends ViewMvc {
    interface MovieListViewListener {

        /**
         * This callback will be invoked when movie card is being clicked
         *
         * @param movieID the id of clicked movie.
         */
        void onClickMovie(int movieID);
    }

    /**
     * Add movie to the view.
     *
     * @param movie the movie to show
     */
    void bindMovie(MovieListObject movie);

    /**
     * This method will be called, when object has been updated.
     *
     * @param movie movie that has been updated.
     */
    void updateMovie(MovieListObject movie);

    /**
     * Set a listener that will be notified by this MVC view
     *
     * @param listener listener that should be notified; null to clear
     */
    void setListener(MovieListViewListener listener);

}
