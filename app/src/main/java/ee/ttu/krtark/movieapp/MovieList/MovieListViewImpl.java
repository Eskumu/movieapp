package ee.ttu.krtark.movieapp.MovieList;

import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ee.ttu.krtark.movieapp.R;

/**
 * Created by Kristjan on 17.04.2018.
 */
class MovieListViewImpl implements MovieListView, MovieListRecyclerAdapter.ItemClickListener {

    private final View rootView;
    private final Fragment controller;
    private RecyclerView recyclerView;
    private MovieListRecyclerAdapter adapter;
    private MovieListViewListener listener;

    MovieListViewImpl(LayoutInflater inflater, ViewGroup container, Fragment controller) {
        this.controller = controller;
        rootView = inflater.inflate(R.layout.fragment_movie_list, container, false);

        initialize();
        setupAdapter();
    }

    /**
     * Initialize view components from root view.
     */
    private void initialize() {
        recyclerView = rootView.findViewById(R.id.movie_list_recycler);
    }

    /**
     * Setup recyclerView to hold movies.
     */
    private void setupAdapter() {
        recyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(controller.getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new MovieListRecyclerAdapter(controller.getActivity());
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Add movie to the view.
     *
     * @param movie the movie to show
     */
    @Override
    public void bindMovie(MovieListObject movie) {
        if (adapter == null) {
            return;
        }
        adapter.add(movie);
    }

    /**
     * This method will be called, when object has been updated.
     *
     * @param movie movie that has been updated.
     */
    @Override
    public void updateMovie(MovieListObject movie) {
        int position = adapter.getMoviePosition(movie);
        if (position != -1) {
            adapter.notifyItemChanged(position);
        }
    }

    /**
     * Set a listener that will be notified by this MVC view
     *
     * @param listener listener that should be notified; null to clear
     */
    @Override
    public void setListener(MovieListViewListener listener) {
        this.listener = listener;
    }

    /**
     * Get the root Android View which is used internally by this MVC View for presenting data
     * to the user.<br>
     * The returned Android View might be used by an MVC Controller in order to query or alter the
     * properties of either the root Android View itself, or any of its child Android View's.
     *
     * @return root Android View of this MVC View
     */
    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public void onItemClick(int id) {
        if (listener != null) {
            listener.onClickMovie(id);
        }
    }
}
