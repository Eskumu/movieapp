package ee.ttu.krtark.movieapp.MovieList;

import ee.ttu.krtark.movieapp.DataBaseObjects.MovieCategory;
import ee.ttu.krtark.movieapp.DataBaseObjects.Movie;

public class MovieListObject {

    private int id;
    private String title;
    private int year;
    private int rating;
    private String category = "";
    private int categoryID;

    public MovieListObject(Movie movie) {
        id = movie.getId();
        title = movie.getTitle();
        year = movie.getYear();
        rating = movie.getRating();
        categoryID = movie.getCategoryID();
    }

    public void addCategory(MovieCategory category) {
        this.category = category.getCategoryName();
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public int getRating() {
        return rating;
    }

    public String getCategory() {
        return category;
    }

    public int getCategoryID() {
        return categoryID;
    }
}
