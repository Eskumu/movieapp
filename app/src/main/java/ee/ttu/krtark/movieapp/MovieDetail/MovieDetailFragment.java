package ee.ttu.krtark.movieapp.MovieDetail;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ee.ttu.krtark.movieapp.DataBaseObjects.MovieCategory;
import ee.ttu.krtark.movieapp.MovieDAO;
import ee.ttu.krtark.movieapp.DataBaseObjects.Movie;

/**
 * Fragment to display movie detail.
 */
public class MovieDetailFragment extends Fragment {
    private static final String KEY_MOVIE_ID = "MOVIE_ID";
    private int movieID;
    private MovieDetailViewImpl viewMVC;
    private MovieDAO db;
    private MovieDetailObject movieDetail;


    /**
     * Return new instance of fragment, with given movie ID.
     *
     * @param movieID the ID of the movie that fragment should display.
     * @return fragment instance that displays given movie.
     */
    public static MovieDetailFragment newInstance(int movieID) {
        MovieDetailFragment fragment = new MovieDetailFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_MOVIE_ID, movieID);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args.containsKey(KEY_MOVIE_ID)) {
            movieID = args.getInt(KEY_MOVIE_ID);
        } else {
            throw new IllegalStateException("MovieDetailFragment must be started with movie ID argument");
        }

        viewMVC = new MovieDetailViewImpl(inflater, container);
        db = new MovieDAO() {
            @Override
            public void onMovieReceivedFromDB(Movie movie) {
                addMovie(movie);
            }

            @Override
            public void onCategoryReceivedFromDB(MovieCategory movieCategory) {
                addCategory(movieCategory);
            }
        };
        return viewMVC.getRootView();
    }

    /**
     * Start looking for movie in the database.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db.findMovieByID(movieID);
    }

    /**
     * Add movie to the fragment and start looking for category information.
     */
    public void addMovie(Movie movie) {
        if (movie != null) {
            movieDetail = new MovieDetailObject(movie);
            db.findMovieCategoryByID(movie.getCategoryID());
        }
    }

    /**
     * Add category information to the movie.
     * <p>
     * Now movie should be ready to display.
     */
    public void addCategory(MovieCategory movieCategory) {
        if (movieCategory != null && movieDetail != null) {
            movieDetail.addCategory(movieCategory);
            onMovieReady();
        }
    }

    /**
     * Called when movie is ready to display.
     * <p>
     * Sends signal to view that movie is ready.
     */
    public void onMovieReady() {
        viewMVC.bindMovie(movieDetail);
        viewMVC.onMovieReady();
    }

}
