package ee.ttu.krtark.movieapp;

import android.os.AsyncTask;
import android.support.annotation.VisibleForTesting;
import android.util.SparseArray;

import ee.ttu.krtark.movieapp.DataBaseObjects.MovieCategory;
import ee.ttu.krtark.movieapp.DataBaseObjects.Movie;
import ee.ttu.krtark.movieapp.DataBaseObjects.SampleData;

public abstract class MovieDAO {

    private SparseArray<Movie> sampleMovies = SampleData.getInstance().getSampleMovies();
    private SparseArray<MovieCategory> sampleCategories = SampleData.getInstance().getSampleCategories();

    /**
     * Called when movie is received from db.
     */
    public abstract void onMovieReceivedFromDB(Movie movie);

    /**
     * Called when category is received from db.
     */
    public abstract void onCategoryReceivedFromDB(MovieCategory movieCategory);

    public void findAllMovies() {
        new FindAllMovies().execute();
    }

    public void findMovieByID(int id) {
        new FindMovieByID().execute(id);
    }

    public void findMovieCategoryByID(int id) {
        new FindCategoryByID().execute(id);
    }


    /**
     * Async task for getting all the movies.
     * <p>
     * This class is wrapped with public method {@link #findAllMovies()}
     */
    @VisibleForTesting
    public class FindAllMovies extends AsyncTask<Void, Movie, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i < sampleMovies.size(); i++) {
                try {
                    // to emulate database connection
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Movie movie = sampleMovies.valueAt(i);
                publishProgress(movie);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Movie... movies) {
            onMovieReceivedFromDB(movies[0]);
        }

    }

    /**
     * Async task for finding movie by id.
     * <p>
     * This class is wrapped with public method {@link #findMovieByID(int)} ()}
     */
    private class FindMovieByID extends AsyncTask<Integer, Movie, Movie> {

        @Override
        protected Movie doInBackground(Integer... ids) {
            try {
                // to emulate database connection
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return sampleMovies.get(ids[0], null);
        }

        @Override
        protected void onPostExecute(Movie movie) {
            onMovieReceivedFromDB(movie);
        }

    }

    /**
     * Async task for finding category by id.
     * <p>
     * This class is wrapped with public method {@link #findMovieCategoryByID(int)} ()}
     */
    private class FindCategoryByID extends AsyncTask<Integer, MovieCategory, MovieCategory> {

        @Override
        protected MovieCategory doInBackground(Integer... ids) {
            try {
                // to emulate database connection
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return sampleCategories.get(ids[0], null);
        }

        @Override
        protected void onPostExecute(MovieCategory category) {
            onCategoryReceivedFromDB(category);
        }
    }

    @VisibleForTesting (otherwise = VisibleForTesting.NONE)
    public void setupForTesting(SparseArray<Movie> movies, SparseArray<MovieCategory> categories) {
        sampleMovies = movies;
        sampleCategories = categories;
    }
}
