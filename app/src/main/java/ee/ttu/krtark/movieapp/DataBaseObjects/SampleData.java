package ee.ttu.krtark.movieapp.DataBaseObjects;

import android.util.SparseArray;

public final class SampleData {
    private static final SampleData INSTANCE = new SampleData();
    private final SparseArray<Movie> sampleMovies;
    private final SparseArray<MovieCategory> sampleCategories;

    public static SampleData getInstance() {
        return INSTANCE;
    }

    private SampleData() {
        sampleMovies = makeSampleMovies();
        sampleCategories = makeSampleCategories();
    }

    public SparseArray<Movie> getSampleMovies() {
        return sampleMovies;
    }

    public SparseArray<MovieCategory> getSampleCategories() {
        return sampleCategories;
    }

    /**
     * Make sample movies
     */
    private SparseArray<Movie> makeSampleMovies() {
        SparseArray<Movie> movies = new SparseArray<>();

        String loremIpsum =
                "Qui placeat accusamus aut iusto tenetur. Ut qui non fuga commodi et ad. Dolorem ipsum fugit dolorem.\n" +
                        "\n" +
                        "Necessitatibus tenetur aut aspernatur consequuntur quod dolor quisquam tempore. Possimus ea pariatur porro et. Architecto facere minus sed. Sunt aliquid dolor quas nulla error consequuntur cupiditate quia.\n" +
                        "\n" +
                        "Sunt mollitia placeat ea quo placeat. Non vel aut dolorum asperiores voluptatem est et sunt. Est inventore enim perspiciatis repellat quidem temporibus fuga. Eum culpa quibusdam distinctio sed ut sit ipsa quis. Repellat rerum sed non pariatur. Omnis quaerat qui voluptatum.\n" +
                        "\n" +
                        "Voluptatem enim esse nisi voluptatem nam dolorem tempora exercitationem. Sit dignissimos quas pariatur qui libero. Tempora a nihil eos. Asperiores molestiae et et quia. Corrupti autem amet officia aut numquam qui repudiandae tempore. Sed voluptas laudantium possimus quidem.\n" +
                        "\n" +
                        "Beatae vel voluptas repudiandae cum provident. Et deserunt sit voluptatem itaque. Dolor repellendus dolor id repellendus a aut quis molestias.\n";

        Movie movie1 = new Movie();
        movie1.setTitle("The Shawshank Redemption");
        movie1.setDescription(loremIpsum);
        movie1.setRating(5);
        movie1.setYear(1994);
        movie1.setId(1);
        movie1.setCategoryID(1);

        Movie movie2 = new Movie();
        movie2.setTitle("The movie");
        movie2.setDescription(loremIpsum);
        movie2.setRating(5);
        movie2.setYear(2010);
        movie2.setId(2);
        movie2.setCategoryID(1);

        Movie movie3 = new Movie();
        movie3.setTitle("The Godfather");
        movie3.setDescription(loremIpsum);
        movie3.setRating(2);
        movie3.setYear(1972);
        movie3.setId(3);
        movie3.setCategoryID(1);

        Movie movie4 = new Movie();
        movie4.setTitle("Schindler's List");
        movie4.setDescription(loremIpsum);
        movie4.setRating(4);
        movie4.setYear(1993);
        movie4.setId(4);
        movie4.setCategoryID(2);

        Movie movie5 = new Movie();
        movie5.setTitle("The Lord of the Rings: The Return of the King");
        movie5.setDescription(loremIpsum);
        movie5.setRating(4);
        movie5.setYear(2003);
        movie5.setId(5);
        movie5.setCategoryID(2);

        movies.put(movie1.getId(), movie1);
        movies.put(movie2.getId(), movie2);
        movies.put(movie3.getId(), movie3);
        movies.put(movie4.getId(), movie4);
        movies.put(movie5.getId(), movie5);


        return movies;
    }


    /**
     * Make sample movie categories.
     */
    private SparseArray<MovieCategory> makeSampleCategories() {
        SparseArray<MovieCategory> categories = new SparseArray<>();

        MovieCategory action = new MovieCategory();
        action.setId(1);
        action.setCategoryName("Action");

        MovieCategory drama = new MovieCategory();
        drama.setId(2);
        drama.setCategoryName("Drama");

        categories.put(action.getId(), action);
        categories.put(drama.getId(), drama);

        return categories;
    }
}
