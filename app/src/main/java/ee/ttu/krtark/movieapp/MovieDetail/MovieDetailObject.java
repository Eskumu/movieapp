package ee.ttu.krtark.movieapp.MovieDetail;

import ee.ttu.krtark.movieapp.MovieList.MovieListObject;
import ee.ttu.krtark.movieapp.DataBaseObjects.Movie;


public class MovieDetailObject extends MovieListObject {

    private String description;

    MovieDetailObject(Movie movie) {
        super(movie);
        description = movie.getDescription();
    }

    public String getDescription() {
        return description;
    }

}
