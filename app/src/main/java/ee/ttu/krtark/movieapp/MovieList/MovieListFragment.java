package ee.ttu.krtark.movieapp.MovieList;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import ee.ttu.krtark.movieapp.MainActivity;
import ee.ttu.krtark.movieapp.DataBaseObjects.MovieCategory;
import ee.ttu.krtark.movieapp.MovieDAO;
import ee.ttu.krtark.movieapp.MovieDetail.MovieDetailFragment;
import ee.ttu.krtark.movieapp.DataBaseObjects.Movie;


public class MovieListFragment extends Fragment implements MovieListView.MovieListViewListener {

    private MovieListViewImpl viewMVC;
    private MovieDAO db;
    private List<MovieListObject> moviesNoCategory = new LinkedList<>();
    private SparseArray<MovieCategory> categories = new SparseArray<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        viewMVC = new MovieListViewImpl(inflater, container, this);
        viewMVC.setListener(this);
        db = new MovieDAO() {
            @Override
            public void onMovieReceivedFromDB(Movie movie) {
                addMovie(movie);
            }

            @Override
            public void onCategoryReceivedFromDB(MovieCategory category) {
                updateMoviesCategories(category);
            }
        };

        return viewMVC.getRootView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db.findAllMovies();
    }

    /**
     * Adds movie to the view and starts getting movie category info.
     *
     * @param movie the movie to add.
     */
    private void addMovie(Movie movie) {
        MovieListObject listObject = new MovieListObject(movie);
        moviesNoCategory.add(listObject);
        viewMVC.bindMovie(listObject);
        addCategoryToMovie(movie.getCategoryID());
    }


    /**
     * Add category information to movie.
     * <p>
     * If category has already asked from database method updates movie category with information
     * in the categories map.
     * Else it queries the database to update categories.
     *
     * @param categoryID the ID of category that should be added to movies.
     */
    private void addCategoryToMovie(int categoryID) {
        if (categories.get(categoryID) != null) {
            updateMoviesCategories(categories.get(categoryID));
        } else {
            db.findMovieCategoryByID(categoryID);
        }
    }


    /**
     * Updates movie category.
     * <p>
     * Notifies view that movie is updated.
     *
     * @param category the category to add to movies.
     */
    private void updateMoviesCategories(MovieCategory category) {
        categories.put(category.getId(), category);
        Iterator<MovieListObject> movieIterator = moviesNoCategory.iterator();
        while (movieIterator.hasNext()) {
            MovieListObject movie = movieIterator.next();
            if (movie.getCategoryID() == category.getId() && movie.getCategory().equals("")) {
                movie.addCategory(category);
                viewMVC.updateMovie(movie);
                movieIterator.remove();
            }
        }
    }


    /**
     * This callback will be invoked when movie card is being clicked
     *
     * @param movieID the id of clicked movie.
     */
    @Override
    public void onClickMovie(int movieID) {
        ((MainActivity) getActivity()).replaceFragmentAddStack(MovieDetailFragment.newInstance(movieID));
    }
}
