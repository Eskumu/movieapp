package ee.ttu.krtark.movieapp.MovieList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import ee.ttu.krtark.movieapp.R;

/**
 * Created by Kristjan on 17.04.2018.
 */
class MovieListRecyclerAdapter extends RecyclerView.Adapter<MovieListRecyclerAdapter.ViewHolder> {

    private ArrayList<MovieListObject> data;
    private LayoutInflater inflater;
    private ItemClickListener clickListener;

    MovieListRecyclerAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.data = new ArrayList<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView movieTitleView;

        private final TextView movieCategoryView;
        private final TextView movieYearView;
        private final RatingBar movieRatingView;
        private final CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.movie_list_item_card);
            movieTitleView = itemView.findViewById(R.id.movie_list_item_title);
            movieCategoryView = itemView.findViewById(R.id.movie_list_item_category);
            movieYearView = itemView.findViewById(R.id.movie_list_item_year);
            movieRatingView = itemView.findViewById(R.id.movie_list_item_rating);
            itemView.setOnClickListener(this);
        }

        /**
         * Called when a view has been clicked.
         *
         * @param view The view that was clicked.
         */
        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                clickListener.onItemClick((Integer) cardView.getTag());
            }
        }

    }

    @NonNull
    @Override
    public MovieListRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_movie_list_cell, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieListRecyclerAdapter.ViewHolder holder, int position) {
        MovieListObject movie = data.get(position);
        String title = movie.getTitle();
        String category = movie.getCategory();
        int rating = movie.getRating();
        String year = String.valueOf(movie.getYear());
        int id = movie.getId();

        final int bigTitle = 40;
        final int smallerText = 20;
        if (title.length() > bigTitle) {
            holder.movieTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, smallerText);
        }

        holder.movieTitleView.setText(title);
        holder.movieCategoryView.setText(category);
        holder.movieRatingView.setRating(rating);
        holder.movieYearView.setText(year);
        holder.cardView.setTag(id);
    }


    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return data.size();
    }

    public void add(MovieListObject movie) {
        data.add(movie);
        notifyDataSetChanged();
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    /**
     * Return the position of given movie or -1.
     */
    public int getMoviePosition(MovieListObject movie) {
        return data.indexOf(movie);
    }

    public interface ItemClickListener {

        void onItemClick(int id);
    }
}
