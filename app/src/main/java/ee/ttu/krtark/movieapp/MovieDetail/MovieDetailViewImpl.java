package ee.ttu.krtark.movieapp.MovieDetail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import ee.ttu.krtark.movieapp.R;

class MovieDetailViewImpl implements MovieDetailView {


    private final View rootView;
    private TextView titleView;
    private TextView categoryView;
    private TextView yearView;
    private TextView descriptionView;
    private RatingBar ratingView;
    private ProgressBar progressBar;
    private ScrollView detailContainer;

    MovieDetailViewImpl(LayoutInflater inflater, ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        initialize();
    }

    /**
     * Initialize view components from root view.
     */
    private void initialize() {
        progressBar = rootView.findViewById(R.id.movie_detail_progress_bar);
        detailContainer = rootView.findViewById(R.id.movie_detail_container);
        titleView = rootView.findViewById(R.id.movie_detail_title);
        categoryView = rootView.findViewById(R.id.movie_detail_category);
        yearView = rootView.findViewById(R.id.movie_detail_year);
        descriptionView = rootView.findViewById(R.id.movie_detail_description);
        ratingView = rootView.findViewById(R.id.movie_detail_rating);
    }

    /**
     * Add movie to the view.
     *
     * @param movie the movie to show
     */
    @Override
    public void bindMovie(MovieDetailObject movie) {
        String title = movie.getTitle();
        String category = movie.getCategory();
        int rating = movie.getRating();
        String year = String.valueOf(movie.getYear());
        String description = movie.getDescription();

        titleView.setText(title);
        categoryView.setText(category);
        yearView.setText(year);
        descriptionView.setText(description);
        ratingView.setRating(rating);
    }

    /**
     * Action to be taken, when movie is ready to display.
     * <p>
     * Should be called after bindMovie
     */
    @Override
    public void onMovieReady() {
        progressBar.setVisibility(View.GONE);
        detailContainer.setVisibility(View.VISIBLE);
    }

    /**
     * Get the root Android View which is used internally by this MVC View for presenting data
     * to the user.<br>
     * The returned Android View might be used by an MVC Controller in order to query or alter the
     * properties of either the root Android View itself, or any of its child Android View's.
     *
     * @return root Android View of this MVC View
     */
    @Override
    public View getRootView() {
        return rootView;
    }

}
