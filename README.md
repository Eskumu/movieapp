# Movie app

> Simple movie app to view information about different movies.

By [Kristjan T�rk](https://www.dropbox.com/s/26i841dwauta8rh/CV%20Kristjan%20T%C3%A4rk%20%28EST%29.pdf?dl=0)

## Installation instructions

1. Install and open [Android Studio](https://developer.android.com/studio/index.html).

2. Import the git project to Android studio.

   ![img](_images/GitChechout.png)

   ![GitChechout](_images/GitDialog.png)

3. The gradle should start building the project.

4. After gradle has built the project run "Sync project with gradle files"

   ![img](_images/GradleSync.png)

5. Run the app ("Shift" + F10)

## Tasks

### Task 1 - Creating the list view

1. Creating data model classes.
   To accomplish this task I created class `Movie` inside DataBaseObjects packet.
   I Also created `MovieDetailObject` and `MovieListObject` in MovieDetail and MovieList packets. These classes are similar to `Movie`.
   `MovieListObject` doesn't include description, because this is not shown in the list. The constructor accepts `Movie` as a parameter. This class has functionality to add category information (as a String) to the objects via passing a `MovieCategory` object with `addCategory` method.
   `MovieDetailObject` is  extending `MovieListObject`, adding description field.

2. Creating data model class.
   Created `MovieCategory` class inside DataBaseObjects packet.
   For providing sample data I also created `SampleData` class. This class uses `SparseArray` to hold sample movie and category data. This is a array that can have caps in the indices. I use it to map movie and category ID to appropriate instance.

3. Creating a table that shows a list of movies.
   I decided to use Fragments. I created packet `MovieList` inside it I created `MovieListFragment` and `MovieListView`. 
   `MovieListFragment` is the controller, that gets movie data from the database.  `onViewCreated` method starts querying for all the movies. movies that have found are  added to the view via `addMovie` method. This method tells the view to show `MovieListObject` and also calls `addCategoryToMovie` for adding category information.  `addCategoryToMovie` checks if category is already in the cache  and updates movie if it is in the cache, if category is not yet cached it asks database for the information. The category information is added to the movie via `updateMoviesCategories`. This method accepts category as a parameter and updates all movies that category information is not jet set.
   `MovieListViewImpl` is the view, that displays movie data. In the constructor it inflates `fragment_movie_list.xml` and creates adapter for showing cards with movie information. 
   `MovieListRecyclerAdapter` creates new cards with movie information, it uses `fragment_movie_list_cell.xml` to create this card. It also sets onClickListener to the cards. This listener is passed to controller, where appropriate `MovieDetailFragment` is made.

   ![list](_images/MovieList.png)

### Task 2 - Creating the details view

1. Create a detail view for the movies.
   Again I used Fragment to display movie detail. I created `MovieDetailObject`, `MovieDetailFragment` and `MovieDetailView` inside of MovieDetail packet.
   `MovieDetailFragment` must be made with `newInstance` method. This method creates fragment with movieID set in the bundle.  Once view is created fragment starts to query information about the movie. If information is ready to display, it calls `movieDetailViev` to show movie data.
   `MovieDetailView`  inflates `fragment_movie_detail.xml` . Before getting movie from the controller, it displays loading screen. Once movie is ready controller calls `onMovieReady` method and loading screen is switched with detail screen.

2. Add navigation functionality so it is possible to move from a movie in the list to the details view and back.
   Since every Android device has a dedicated back button I decided to use this instead of making my own back button. For providing this functionality I switched from `MovieListFragment` to `MovieDetailFragment` using `MainActivity` method `replaceFragmentAddStack`. This method replaces fragments with adding them to back stack. If user goes to detail fragment back button pops the fragment stack and displays movie list again.

   ![detail](_images/MovieDetail.png)

### Task 3 - Content provider & Service

For this task I decided to use AsyncTask. I created abstract class `MovieDAO`. that has public methods `findAllMovies()`, `findMovieByID(int id)` and `findMovieCategoryByID(int id)` , these public methods starts async tasks. The tasks are made with private classes, if these classes have some progress to publish they call out abstract methods `onMovieReceivedFromDB(Movie movie)` or `onCategoryReceivedFromDB(MovieCategory movieCategory)` . 
These abstract methods has to be implemented when new `MovieDAO` is made.  This way I could hold all the complicated code with asyncTask in the `MovieDAO` while providing flexibility to use results of task differently on every view.
AsyncTasks are set to sleep for 100 milliseconds to emulate database connection.
The main problem is that google recommends that AsyncTask classes should be static, but making them static would make them not so easily reusable.

### Task 4 - Tests

Write local tests for the AsyncTask.

While writing tests I had to think about solutions for emulating AsyncTask and SparseArray. 

For `AsyncTask` I found a solution from this [blog post](http://www.ryanharter.com/blog/2015/12/28/dealing-with-asynctask-in-unit-tests/). The post provides code for making asyncTask run synchronously. making  `execute()` return the result.

For `SparseArray` I created class that uses `LinkedHashMap` internally to emulate `SparseArray`.

I also used `@VisibleForTesting` annotation to make classes visible while testing. Using this annotation I also created method `setupForTesting(SparseArray<Movie> movies, SparseArray<MovieCategory> categories) ` inside `MovieDAO`. This method is not existing if `MovieDAO` is created while not testing.

Writing tests is something I have not done much before and I definitely learned a lot from this.

## Time tracking

Thanks to making Android project to get bonus points in my university (TT�) Java course I got some sample code that simplified making this project and saved me a lot of time.  I created a table showing approximate time I spent on this project.

| Task                                                         | Time spent |
| ------------------------------------------------------------ | ---------- |
| Creating Movie and MovieCategory objects                     | 0.5 h      |
| Creating Sample data and simple MovieDAO (just returning sample data) | 0.25 h     |
| Creating layout files                                        | 1 h        |
| Creating MovieListFragment and View with simple database     | 2 h        |
| Creating MovieDetailFragment with simple database            | 1 h        |
| Creating AsyncTask for getting data from sample data         | 2 h        |
| Migrating Fragments from simple MovieDAO to more complex one | 2 h        |
| Creating Tests for AsyncTasks (hardest part in my opinion)   | 4 h        |
| Writing readme and adding project to Bitbucket               | 2 h        |
| **Total time spent**                                         | **~15 h**  |